import React, { Component } from 'react';
import FormComponent from './Form';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <FormComponent />
    );
  }
}

export default App;
