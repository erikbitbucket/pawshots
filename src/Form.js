import React, { Component } from 'react';
import logo from './logo_v2.svg';
import './Form.css';

class FormComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      firstDogName: '',
      name: '',
      shareholders: [{ name: '' }],
    };
  }

  // handleChange = (event) => {
  //   // If you are using babel, you can use ES 6 dictionary syntax
  //   // let change = { [e.target.name] = e.target.value }
  //   let change = {}
  //   change[event.target.name] = event.target.value
  //   this.setState(change)
  // }

  handleNameChange = (evt) => {
    this.setState({ name: evt.target.value });
  }

  handleShareholderNameChange = (idx) => (evt) => {
    const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
      if (idx !== sidx) return shareholder;
      return { ...shareholder, name: evt.target.value };
    });

    this.setState({ shareholders: newShareholders });
  }

  handleSubmit = (evt) => {
    const { name, shareholders } = this.state;
    alert(`Incorporated: ${name} with ${shareholders.length} shareholders`);
  }

  handleAddShareholder = () => {
    this.setState({
      shareholders: this.state.shareholders.concat([{ name: '' }])
    });
  }

  handleRemoveShareholder = (idx) => () => {
    this.setState({
      shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx)
    });
  }

  // handleSubmit = (event) => {
  //   alert("your submit data: " + this.state.firstName + ", " + this.state.lastName);
  //   event.preventDefault();
  // }

  render() {
    return (
      <div className="form">
        <header className="form-header">
          <img src={logo} className="form-logo" alt="logo" />
          <h3 className="form-title">Waitlist</h3>
        </header>

        <form onSubmit={this.handleSubmit} className="input-form">

          <input className="input-half-l" type="text" onChange={this.handleChange} value={this.state.firstName} name="firstName" placeholder={'First Name'} />
          
          <input className="input-half-r" type="text" onChange={this.handleChange} value={this.state.lastName} name="lastName" placeholder={'Last Name'} />
          
          <input className="input-full email-border" type="text" onChange={this.handleChange} value={this.state.email} name="email" placeholder={'Email'} />
          
          <input className="input-full" type="text" onChange={this.handleChange} value={this.state.phone} name="phone" placeholder={'Phone'} />

          <input className="input-full" type="text" onChange={this.handleChange} value={this.state.firstDogName} name="firstDogName" placeholder={'1 Dog Name'} />

          {this.state.shareholders.map((shareholder, idx) => (
            <div className="shareholder">
              <input className="input-full" type="text" placeholder={`${idx + 2} Dogs Name`} value={shareholder.name} onChange={this.handleShareholderNameChange(idx)} />
              <button className="small" type="button" onClick={this.handleRemoveShareholder(idx)}>X</button>
            </div>
          ))}
          <button className="btn-full" type="button" onClick={this.handleAddShareholder}>Add Additional Photo</button>

          <button className="btn-submit" type="submit" value="Submit">Submit(Total is $20)</button>

        </form>

        <footer className="form-footer">
          <h3 className="footer-title">Menu Bar (Below the fold)</h3>
        </footer>

      </div>
    );
  }
}

export default FormComponent;
